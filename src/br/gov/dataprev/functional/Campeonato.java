package br.gov.dataprev.functional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Campeonato {

	public static void mostrarTodos(List<Time> times) {
		for (Time time : times) {
			System.out.println(time);
		}
		System.out.println("-------------------------------------");
	}
	
	public static List<Time> getTimesDoEstadoCeara(List<Time> in) {
		List<Time> out = new ArrayList<>();
		for (Time time : in) {
			if (time.getEstado().equals("CE")) {
				out.add(time);
			}
		}
		return out;
	}

	public static void main(String[] args) {
		List<Time> times = Arrays.asList(
			Time.withNomeTitulosEstado("Ceara", 14, "CE", "Cearense", "Copa do Nordeste", "Copa do Brasil", "Serie A"),
			Time.withNomeTitulosEstado("Fortaleza", 8, "CE", "Cearense", "Serie B"),
			Time.withNomeTitulosEstado("ABC", 11, "RN", "Potiguar", "Copa do Nordeste", "Copa do Brasil", "Serie C"),
			Time.withNomeTitulosEstado("America", 9, "RN", "Potiguar", "Copa do Brasil", "Serie D"),
			Time.withNomeTitulosEstado("Confianca", 10, "SE", "Sergipano", "Copa do Nordeste", "Copa do Brasil", "Serie C"),
			Time.withNomeTitulosEstado("Botafogo", 8, "PB", "Paraibano", "Copa do Nordeste", "Copa do Brasil", "Serie C"),
			Time.withNomeTitulosEstado("Nautico", 12, "PE", "Pernambucano", "Copa do Nordeste", "Copa do Brasil", "Serie C"),
			Time.withNomeTitulosEstado("Santa Cruz", 6, "PE", "Pernambucano", "Copa do Nordeste", "Copa do Brasil", "Serie C"),
			Time.withNomeTitulosEstado("Sampaio Correa", 7, "MA", "Maranhese", "Copa do Nordeste", "Copa do Brasil", "Serie B"),
			Time.withNomeTitulosEstado("Moto Clube", 5, "MA", "Maranhese", "Serie D")
		);
		
		mostrarTodos(times);
		
		mostrarTodos(getTimesDoEstadoCeara(times));
		
		mostrarTodos(times);
	}
}
