package br.gov.dataprev.functional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Time {
	
	private String nome;
	
	private int titulos;
	
	private String estado;
	
	private List<String> campeonatos;
	
	private Time(String nome, int titulos, String estado, List<String> campeonatos) {
		this.nome = nome;
		this.titulos = titulos;
		this.estado = estado;
		this.campeonatos = campeonatos;
	}
	
	public static Time withNomeTitulosEstado(String nome, int titulos, String estado) {
		return new Time(nome, titulos, estado, null); 
	}
	
	public static Time withNomeTitulosEstado(String nome, int titulos, String estado, String... campeonatos) {		
		return new Time(nome, titulos, estado, Collections.unmodifiableList(Arrays.asList(campeonatos))); 
	}

	public String getNome() {
		return nome;
	}

	public int getTitulos() {
		return titulos;
	}

	public String getEstado() {
		return estado;
	}

	public List<String> getCampeonatos() {
		return campeonatos;
	}

	@Override
	public String toString() {
		return "Time [nome=" + nome + ", titulos=" + titulos + ", estado=" + estado + ", campeonatos=" + campeonatos
				+ "]";
	}
}
